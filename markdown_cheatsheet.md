MARKDOWN CHEATSHEET
===================

Available on [Github][]

[Github]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

#Headers
=======

# H1
## H2
### H3
#### H4
##### H5
###### H6

Alternatively, for H1 and H2, an underline-ish style:

Alt-H1
======

Alt-H2
------

#Emphasis
========

Emphasis, aka italics, with *asterisks* or _underscores_.

Strong emphasis, aka bold, with **asterisks** or __underscores__.

Combined emphasis with **asterisks and _underscores_**.

Strikethrough uses two tildes. ~~Scratch this.~~

#Lists
=====

1. First ordered list item
2. Another item
  * Unordered sub-list.
1. Actual numbers don't matter, just that it's a number
  1. Ordered sub-list
4. And another item.

   Some text that should be aligned with the above item.

* Unordered list can use asterisks
- Or minuses
+ Or pluses

#Links
=====

[I'm an inline-style link](https://www.google.com)

[I'm a reference-style link][Arbitrary case-insensitive reference text]

[You can use numbers for reference-style link definitions][1]

Or leave it empty and use the [link text itself][]

Some text to show that the reference links can follow later.

[arbitrary case-insensitive reference text]: https://www.mozilla.org
[1]: http://slashdot.org
[link text itself]: http://www.reddit.com

#Images
======

Here's our logo (hover to see the title text):

Inline-style:
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

Reference-style:
![alt text][logo]

[logo]: https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 2"

#Code and Syntax Highlighting
============================

Inline `code` has `back-ticks around` it.

Blocks of code are either fenced by lines with three back-ticks ```,
or are indented with four spaces. I recommend only using the fenced code
blocks -- they're easier and only they support syntax highlighting.

    ```javascript
    var s = "JavaScript syntax highlighting";
    alert(s);
    ```

    ```python
    s = "Python syntax highlighting"
    print s
    ```

    ```
    No language indicated, so no syntax highlighting.
    But let's throw in a <b>tag</b>.
         ```

#Blockquotes
===========

> Blockquotes are very handy in email to emulate reply text.
> This line is part of the same quote.

#Quote break.
===========

> This is a very long line that will still be quoted properly when it wraps. Oh boy let's keep writing to make sure this is long enough to actually wrap for everyone. Oh, you can *put* **Markdown** into a blockquote.


#Horizontal Rule
===============

Three or more...

----
Hyphens

****
Asterisks

___
Underscores


#Line Breaks
===========

My basic recommendation for learning how line breaks work is to experiment
and discover -- hit <Enter> once (i.e., insert one newline), then hit it
twice (i.e., insert two newlines), see what happens. You'll soon learn to
get what you want. "Markdown Toggle" is your friend.
